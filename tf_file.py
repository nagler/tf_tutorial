import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn import preprocessing
import sys

FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer('type', 0, """Linear Regression/ Logistic Regression""")
tf.app.flags.DEFINE_string('train_dir', '/tf_model',
                           """Directory where to write event logs """
                           """and checkpoint.""")
tf.app.flags.DEFINE_string('data_dir', '/data',
                           """Directory to data""")
tf.app.flags.DEFINE_integer('epochs', 100,
                            """Number of batches to run.""")
tf.app.flags.DEFINE_integer('log_frequency', 10,
                            """How often to log results to the console.""")
tf.app.flags.DEFINE_integer('batch_size', 64,
                            """Size of batch""")


def run_regression():
    path = FLAGS.data_dir
    df = pd.read_csv(path)
    df['bias'] = 1
    df['MinTemp'] = preprocessing.scale(df['MinTemp'])
    df = df[['MinTemp', 'bias', 'MaxTemp']]
    features, labels = (df[['MinTemp', 'bias']].values,
                        df['MaxTemp'].values.reshape(-1, 1))
    # dataset = tf.data.Dataset.from_tensor_slices((features, labels)).repeat().batch(FLAGS.batch_size)
    # iter = dataset.make_one_shot_iterator()
    #ds = df_to_dataset(df, shuffle=False, batch_size=FLAGS.batch_size)
    #iter = ds.make_one_shot_iterator
    N = len(df)
    X = tf.placeholder(dtype=tf.float32, name='X', shape=[None, 2])
    Y = tf.placeholder(dtype=tf.float32, name='Y', shape=[None, 1])
    W = tf.Variable(np.random.rand(2, 1).astype(np.float32), name='weights')
    Z = tf.matmul(X, W)
    error = Y - Z
    costw = tf.reduce_sum(tf.pow(error, 2)) / (2 * N)

    optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-1)
    saver = tf.train.Saver()
    writer = tf.summary.FileWriter('./graphs', tf.get_default_graph())

    train_op = optimizer.minimize(costw)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for epoch in range(FLAGS.epochs):
            # Train step
            num_iter = N/FLAGS.batch_size
            # x,y = iter.get_next()
            for i in range(num_iter):
                # print i
                x, y = features[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size], labels[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size]
                # x, y = iter.get_next()
                # x, y = x.eval(), y.eval()
                # print x, y
                # print x, y
                sess.run(train_op, feed_dict={X: x, Y: y})

            # Logs
            if (epoch + 1) % FLAGS.log_frequency == 0:
                # Save intermediate weights
                saver.save(sess, './model/', global_step=epoch)
                x = features.astype(np.float32)
                y = labels.astype(np.float32)
                c = sess.run(costw, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
                print("checkpoint saved. Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(c),
                      "W=", sess.run(W))

        print("Optimization Finished!")
        x = features.astype(np.float32)
        y = labels.astype(np.float32)
        training_cost = sess.run(costw, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
        print("Evaluation cost=", training_cost, "W=", sess.run(W), '\n')
        saver.save(sess, './model/', global_step=epoch)
        weights = sess.run(W)


    writer.close()
    fig = plt.figure()
    plt.plot(features, labels, 'ro')
    plt.plot(features, np.matmul(features, weights), color='b')
    plt.show()

def run_clasifier():

    path = FLAGS.data_dir
    df = pd.read_csv(path)
    df['bias'] = 1
    df = df[['variance', 'skewness', 'curtosis', 'entropy', 'bias', 'authenticity']]
    features, labels = (df[['variance', 'skewness', 'curtosis', 'entropy', 'bias']].values,
                        df['authenticity'].as_matrix().reshape(-1, 1))
    feat_size = features.shape[1]
    # dataset = tf.data.Dataset.from_tensor_slices((features, labels)).repeat().batch(FLAGS.batch_size)
    # iter = dataset.make_one_shot_iterator()
    # ds = df_to_dataset(df, shuffle=False, batch_size=FLAGS.batch_size)
    # iter = ds.make_one_shot_iterator
    N = len(df)
    X = tf.placeholder(dtype=tf.float32, name='X', shape=[None, feat_size])
    Y = tf.placeholder(dtype=tf.float32, name='Y', shape=[None, 1])
    W = tf.Variable(np.random.rand(feat_size, 1).astype(np.float32), name='weights')
    Z = tf.math.sigmoid(tf.matmul(X, W))
    #error = tf.losses.sigmoid_cross_entropy(Y, Z)
    error = -tf.reduce_mean(((Y*tf.log(Z))+((1-Y)*tf.log(1-Z))))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.05)
    saver = tf.train.Saver()
    writer = tf.summary.FileWriter('./graphs', tf.get_default_graph())

    train_op = optimizer.minimize(error)

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for epoch in range(FLAGS.epochs):
            # Train step
            num_iter = N / FLAGS.batch_size

            # x,y = iter.get_next()
            for i in range(num_iter):
                # print i
                x, y = features[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size], labels[i * FLAGS.batch_size: i * FLAGS.batch_size + FLAGS.batch_size]
                # x, y = iter.get_next()
                # x, y = x.eval(), y.eval()
                # print x, y
                # print x, y
                sess.run(train_op, feed_dict={X: x, Y: y})

            # Logs
            if (epoch + 1) % FLAGS.log_frequency == 0:
                # Save intermediate weights
                saver.save(sess, './model/', global_step=epoch)
                x = features.astype(np.float32)
                y = labels.astype(np.float32)
                c = sess.run(error, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
                print("checkpoint saved. Epoch:", '%04d' % (epoch + 1), "cost=", "{:.9f}".format(c),
                      "W=", sess.run(W))

        print("Optimization Finished!")
        x = features.astype(np.float32)
        y = labels.astype(np.float32)
        training_cost = sess.run(error, feed_dict={X: x, Y: y})  ### usualy we use here EVALUATION data
        print("Evaluation cost=", training_cost, "W=", sess.run(W), '\n')
        saver.save(sess, './model/', global_step=epoch)
        weights = sess.run(W)
        prob = sess.run(Z, feed_dict={X: x, Y: y})
        correct_pred = tf.equal(tf.round(prob), Y)
        accuracy = sess.run(tf.reduce_mean(tf.cast(correct_pred, tf.float32)), feed_dict={X: x, Y: y})

        print "Accuracy: ",accuracy


def main(argv=None):  # pylint: disable=unused-argument
    # if tf.gfile.Exists(FLAGS.train_dir):
    #   tf.gfile.DeleteRecursively(FLAGS.train_dir)
    # tf.gfile.MakeDirs(FLAGS.train_dir)
    if FLAGS.type:
        run_clasifier()
    else:
        run_regression()

if __name__ == '__main__':
    print sys.path
    tf.app.run()
